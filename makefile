clean:
	find . -name *.vtu -exec rm '{}' ';'
	find . -name __pycache__ -exec rm -rf '{}' ';'
