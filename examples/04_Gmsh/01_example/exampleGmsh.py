import sys
sys.path.append('../../../src')

from fem import Mesh, Model
from collections import defaultdict
from elements.mechanics import MechanicalCreator
#from importGmsh2 import importGmsh

#from gmshModel.Model import *
from gmsh import importGmsh
def createModel():
    """
    create a model by reading a .msh file built with gmsh, and convert it
    to pyris format.
    """

    vsetsNames = ['top','bottom']
    cellsetsNames = ['allcells']
    (vertices, cells, vsets, cellsets) = importGmsh("./examples/test.msh")
    
    print("\n-----------------------------------------------------------")
    print("Model imported from Gmsh")
    print("\n-----------------------------------------------------------")
    print("Sets:")
    for s in cellsets:
        print("   ", s, ", size: ", len(cellsets[s]))
                
    theMesh = Mesh(vertices, cells, vsets, cellsets)
    theMesh.print()

    elmtTypes = {}
    elmtTypes["allcells"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})


    # constrain a nodeset: (doflabel, value) for all nodes in the set
    constraints = defaultdict(list) # do not change this line
    constraints["bottom"].append((0, 0.0))
    constraints["bottom"].append((1, 0.0))
    constraints["bottom"].append((2, 0.0))


    # load a nodeset: (doflabel, value) for all nodes in the set
    loading = defaultdict(list) # do not change this line
    loading["top"].append((0, -10.0))
    #loading["top"].append((1, 20.0))
    #loading["top"].append((2, 30.0))

    theModel = Model(theMesh, elmtTypes, constraints, loading)
    return theModel
