import sys
sys.path.append('../../../src')

from fem import Mesh, Model, StaticLinearAnalysis
from elements.imechanics import MechanicalCreator



#--------------------------------------------------------
#           mesh geometry: vertices, cells and nodesets
#--------------------------------------------------------
vertices = [
    (0.0, 0.0),
    (1.0, 0.0),
    (0.0, 1.0),
    (1.0, 1.0),
    (0.0, 2.0),
    (1.0, 2.0)]

cells = [(0, 1, 3),
         (0, 3, 2),
         (4, 2, 3),
         (3, 5, 4)]

# vertexsets, with names and vertex labels
# careful: if only one vertex, use a comma after the vertexlabel
vertexsets = {}
vertexsets["bottom"] = (0, 1)
vertexsets["top"] = (4, 5)

cellsets = {}
cellsets["triangles"] = (0, 1, 2, 3)


# --------------------------------------------------------
#               Create global data structures
# --------------------------------------------------------
theMesh = Mesh(vertices, cells, vertexsets, cellsets)
theMesh.print()


# --------------------------------------------------------
#  Model: elements types, constraints, and loading on the mesh
# --------------------------------------------------------

# assign one element type to each cellset
elmtTypes = {}
elmtTypes["triangles"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3,
                                            "bx" : 17.0, "by" : -400.0})

theModel = Model(theMesh, elmtTypes)

theModel.addConstraint(vertexset="bottom", dof=0, value=0.0)
theModel.addConstraint(vertexset="bottom", dof=1, value=0.0)

theModel.addLoading(vertexset="top", dof=0, value=10000)
theModel.addLoading(vertexset="top", dof=0, value=20000)

analysis = StaticLinearAnalysis(theModel)
analysis.solve()
theModel.printDetailed()
