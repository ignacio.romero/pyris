// -----------------------------------------------------------------------------
//
//  Gmsh GEO
//
// -----------------------------------------------------------------------------

//Use the following line to generate the mesh (.inp (abaqus))
//gmsh mesh.geo  -3 -o mesh.inp

h     =1.0;   //mesh size





// ------------------------------------------------------
// ------------------------------------------------------
// A)Geometry Definition: 1)Points 
//                        2)Lines 
//                        3)Curve 
//                        4)Surface 

// ------------------------------------------------------
// A1)Points Definitions: 
//
//         P4*------------*P3
//           |            |
//           |            |
//           |            |
//           |            |
//           |            |
//           |            |
//           |            |
//           |            |
//         P1*------------*P2
//
//    |Y
//    |
//    ---X   Dimensions:   1 x 0.5 x 0.02
// Z /
//

//           -----Coordinates--
//Points:    ----X,------Y,---Z,
Point(1)   ={  0.0,    0.0,   0,  h};
Point(2)   ={  1.0,    0.0,   0,  h};
Point(3)   ={  1.0,    3.0,   0,  h};
Point(4)   ={  0.0,    3.0,   0,  h};



// ------------------------------------------------------
// A2)Lines Definition
//
//            <-L3
//        *----------*
//        |          |
//        |          ^
//      |L4          |L2
//        |          |
//        |          |
//        *----------*
//           L1->

Line(1) = {1, 2};  //L1:from P1 to P2: P1*--L1-->*P2
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};



// ------------------------------------------------------
// A3)Curve Definition
//
//            
//        *----<-----*
//        |          |
//        |          |
//        |          ^ Curve 5
//        |          | 
//        |          |
//        *----->----*
//   

Curve Loop(5) = {1,2,3,4};  //C5: through lines L1,L2,...,L7

// ------------------------------------------------------
// A4)Surface Definition
//
//        *----------*
//        |          |
//        |          |
//        |          | 
//        |          | 
//        |          |
//        *----------*
//         

Plane Surface(6) = {5}; 
Transfinite Surface {6};


//Plane Surface(6) = {5,6};  //  Curve loop 5 C5 --> Surface S6
Recombine Surface {6};

//Rotate{{0,0,1}, {0,0,0}, Pi/4}{ Surface{6}; }

// ------------------------------------------------------
// ------------------------------------------------------
// B)Mesh Generation: 1)Mesh size Box1 
//                    2)Mesh size Box2
//                    3)Mesh min(Box1,Box2)
//                    3)Extrude Mesh 
//                    4)Mesh Algorithm  


// ------------------------------------------------------
// B1) Mesh size Box1
//
//        *----------------* 
//        |                | 
//        |                |
//        |                |
//        *                |
//             | hcrack F6 |  (Field[6])
//        *     -----------| 
//        |                |
//        |                |
//        |                |
//        *----------------*

//Field[6]      =    Box;
//Field[6].VIn  = hcrack;
//Field[6].VOut =      h;

//Field[6].XMin = -0.50;
//Field[6].XMax =  0.50;
//Field[6].YMin =  0.00;
//Field[6].YMax =  0.25;





// ------------------------------------------------------
// B4)Extrude Mesh

//     {X, Y,    Z}    Surface
Extrude{0, 0,  0.1}{Surface{6}; Layers{1};Recombine;}


// ------------------------------------------------------
// B5)Mesh Algorithm
//Mesh.SubdivisionAlgorithm=2;
Mesh.Algorithm3D   = 1;


Physical Surface("bottom", 29) = {15};
Physical Surface("top", 30) = {23};
