import sys
sys.path.append('../../../src')

from fem import Mesh, Model
from collections import defaultdict
from elements.mechanics import MechanicalCreator
from modelpart import class_modelpart

def createModel():
    """
    The goal of one example is to create a Model. This model will have
    to include the Mesh (with its components - see below), the type of
    elements (the equations that represent its behaviour), and the
    actions on the model (boundary conditions and loading).
    """

    #--------------------------------------------------------
    #           mesh geometry: vertices, cells and nodesets
    #--------------------------------------------------------
    
    modelpart = class_modelpart("mesh.msh")
    
    vertices = modelpart.vertices_current
    cells    = modelpart.cells_current
    
    # vsets, with names and vertex labels
    # careful: if only one vertex, use a comma after the vertexlabel
    # 0 es Physical Surface("bottom", 29) = {15};
    # 2 es Physical Surface("top", 30) = {23};
    # Nota: en proceso (automatizar)!!!!
    vsets = {}
    vsets["bottom"]  = tuple(modelpart.get_physical_surface_cells(0))
    vsets["top"]     = tuple(modelpart.get_physical_surface_cells(2))

    cellsets = {}
    cellsets["tets"] = range(0,len(cells))


    # --------------------------------------------------------
    #               Create global data structures
    # --------------------------------------------------------
    theMesh = Mesh(vertices, cells, vsets, cellsets)
    theMesh.print()


    # --------------------------------------------------------
    #  Model: elements types, constraints, and loading on the mesh
    # --------------------------------------------------------

    # assign one element type to each cellset
    elmtTypes = {}
    elmtTypes["tets"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})

    # constrain a nodeset: (doflabel, value) for all nodes in the set
    constraints = defaultdict(list) # do not change this line
    constraints["bottom"].append((0, 0.0))
    constraints["bottom"].append((1, 0.0))
    constraints["bottom"].append((2, 0.0))

    # load a nodeset: (doflabel, value) for all nodes in the set
    loading = defaultdict(list) # do not change this line
    loading["top"].append((0, 10.0))
    loading["top"].append((1, 20.0))
    loading["top"].append((2, 30.0))

    theModel = Model(theMesh, elmtTypes, constraints, loading)
    return theModel
