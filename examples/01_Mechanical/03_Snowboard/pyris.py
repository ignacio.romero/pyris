#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Thu Sep  1 16:49:17 2022
#
#@author: ignacio romero
#"""
import sys
sys.path.append('../../src')


# this example is a stationary elasticity problem for a geometry
# created with Gmsh.
# To run it, change the keyword "False" to "True"
# in the next line. To use a different model, use another example file

from fem import StaticLinearAnalysis
from exampleSnowboard import createModel

model = createModel()
model.mesh.printCells()
analysis = StaticLinearAnalysis(model)
analysis.solve()
#model.printDetailed()
