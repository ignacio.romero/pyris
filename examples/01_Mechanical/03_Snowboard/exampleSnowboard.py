from fem import Mesh, Model
from collections import defaultdict
from elements.mechanics import MechanicalCreator
from importGmsh import importGmsh
#from importer import gmshImport

def createModel():
    """
    create a model by reading a .msh file built with gmsh, and convert it
    to pyris format.
    """

    # vsetsNames = ['top','bottom']
    # cellsetsNames = ['allcells']
    (vertices, cells, vsets, cellsets) = importGmsh("snowboard.msh")
    #vertices, cells, vsets, cellsets) = gmshImport("./examples/snowboard.msh",
    #debug = false)

    print("\n-----------------------------------------------------------")
    print("Model imported from Gmsh")
    print("-----------------------------------------------------------")
    print("Sets:")
    for s in cellsets:
        print("   ", s, ", size: ", len(cellsets[s]))

    theMesh = Mesh(vertices, cells, vsets, cellsets)
    theMesh.print()

    elmtTypes = {}
    elmtTypes["Volumen1"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})
    elmtTypes["Volumen2"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})
    elmtTypes["Volumen3"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})
    elmtTypes["Volumen4"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})
    elmtTypes["Volumen5"] = MechanicalCreator({"young" : 210e9, "poisson" : 0.3})


    # constrain a nodeset: (doflabel, value) for all nodes in the set
    constraints = defaultdict(list) # do not change this line
    constraints["Apoyo"].append((0, 0.0))
    constraints["Apoyo"].append((1, 0.0))
    constraints["Apoyo"].append((2, 0.0))


    # load a nodeset: (doflabel, value) for all nodes in the set
    loading = defaultdict(list) # do not change this line
    loading["PieDer"].append((0, 0.0))
    loading["PieDer"].append((1, 0.0))
    loading["PieDer"].append((2, -400.0))
    loading["PieIzq"].append((2, -400.0))

    theModel = Model(theMesh, elmtTypes, constraints, loading)
    return theModel
