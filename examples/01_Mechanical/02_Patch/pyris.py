#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Thu Sep  1 16:49:17 2022
#
#@author: ignacio romero
#"""
import sys
sys.path.append('../../../src')


# this example is a stationary elasticity problem of the patch test.
# To run it, change the keyword "False" to "True"
# in the next line. To use a different model, use another example file

from examplePatch import createModel
from fem import StaticLinearAnalysis

model = createModel()
analysis = StaticLinearAnalysis(model)
analysis.solve()
analysis.model.print()
