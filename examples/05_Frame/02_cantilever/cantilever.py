import sys
sys.path.append('../../../src')

from fem import Mesh, Model, StaticLinearAnalysis
from collections import defaultdict
from elements.frame import FrameCreator

#--------------------------------------------------------
#           mesh geometry: vertices, cells and nodesets
#--------------------------------------------------------
L = 2.0
vertices = [
    (0.0, 0.0), # 0
    (0.5, 0.0), # 1
    (1.0, 0.0), # 2
    (1.5, 0.0), # 3
    (2.0, 0.0)] # 4

cells = [
    (0, 1),
    (1, 2),
    (2, 3),
    (3, 4)]

# vertexsets, with names and vertex labels
# careful: if only one vertex, use a comma after the vertexlabel
vertexsets = {}
vertexsets["clamp"] = (0,)
vertexsets["tip"] = (4,)

cellsets = {}
cellsets["beams"] = (0, 1, 2, 3)


# --------------------------------------------------------
#               Create global data structures
# --------------------------------------------------------
theMesh = Mesh(vertices, cells, vertexsets, cellsets)
theMesh.print()


# --------------------------------------------------------
#  Model: elements types, constraints, and loading on the mesh
# --------------------------------------------------------

# assign one element type to each cellset
elmtTypes = {}
elmtTypes["beams"] = FrameCreator({"E" : 210e9,
                                   "A" : 1e-2,
                                   "I" : 3.3e-6,
                                   "density" : 0.0,
                                   "gravity" : 9.8})

theModel = Model(theMesh, elmtTypes)
theModel.addConstraint(vertexset="clamp", dof=0, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=1, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=2, value=0.0)

theModel.addLoading(vertexset="tip", dof=1, value=-10000.0)

# --------------------------------------------------------
#  Analysis: running and finding a solution
# --------------------------------------------------------

analysis = StaticLinearAnalysis(theModel)
analysis.solve()
#theModel.printDetailed()
