import sys
sys.path.append('../../../src')
import math

from fem import *
from collections import defaultdict
from elements.frame import FrameCreator

#--------------------------------------------------------
#           mesh geometry: vertices, cells and nodesets
#--------------------------------------------------------
L = 2.0
vertices = [
    (0.0, 0.0),
    (L/2, 0.0),
    (L, 0.0)]

cells = [(0, 1), (1,2)]

# vertexsets, with names and vertex labels
# careful: if only one vertex, use a comma after the vertexlabel
vertexsets = {}
vertexsets["clamp"] = (0,)
vertexsets["tip"] = (2,)

cellsets = {}
cellsets["beams"] = (0,1)


# --------------------------------------------------------
#               Create global data structures
# --------------------------------------------------------
theMesh = Mesh(vertices, cells, vertexsets, cellsets)
theMesh.print()


# --------------------------------------------------------
#  Model: elements types, constraints, and loading on the mesh
# --------------------------------------------------------

# assign one element type to each cellset
elmtTypes = {}
elmtTypes["beams"] = FrameCreator({"E" : 210e9,
                                   "A" : 1e-2,
                                   "I" : 1e-6,
                                   "W" : 1e-5,
                                   "sigmae": 2e6,
                                   "density" : 400000.0,
                                   "gravity" : 0.0})


theModel = Model(theMesh, elmtTypes)
theModel.addConstraint(vertexset="clamp", dof=0, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=1, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=2, value=0.0)
theModel.addLoading(vertexset="tip", dof=1, value=-20000.0)


analysis = StaticLinearAnalysis(theModel)
analysis.solve()
#model.printDetailed()
