import sys
sys.path.append('../../../src')
import math

from fem import *
from collections import defaultdict
from elements.frame import FrameCreator

#--------------------------------------------------------
#           mesh geometry: vertices, cells and nodesets
#--------------------------------------------------------
L = 2.0
vertices = [
    (0.0, 0.0), # 0
    (  L, 0.0), # 1
    (0.0,   L), # 2
    (  L,   L), # 3
    (0.5*L, 2*L)] # 4

cells = [
    (0, 2),
    (1, 3),
    (2, 4),
    (3, 4),
    (2, 3)]

# vertexsets, with names and vertex labels
# careful: if only one vertex, use a comma after the vertexlabel
vertexsets = {}
vertexsets["clamp"] = (0, 1)
vertexsets["top"] = (4,)

cellsets = {}
cellsets["beams"] = (0, 1, 2, 3, 4)

theMesh = Mesh(vertices, cells, vertexsets, cellsets)
theMesh.print()


# --------------------------------------------------------
#  Model: elements types, constraints, and loading on the mesh
# --------------------------------------------------------

# assign one element type to each cellset
elmtTypes = {}
elmtTypes["beams"] = FrameCreator({"E" : 210e9,
                                   "A" : 1e-2,
                                   "I" : 1e-6,
                                   "W" : 1e-5,
                                   "sigmae": 2e6,
                                   "density" : 4000.0,
                                   "gravity" : 9.8})

theModel = Model(theMesh, elmtTypes)
theModel.addLoading(vertexset="top", dof=0, value=1000.0,
                    scaling="sin(t)*(t<=2)")
theModel.addConstraint(vertexset="clamp", dof=0, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=1, value=0.0)
theModel.addConstraint(vertexset="clamp", dof=2, value=0.0)


# --------------------------------------------------------
#  Analysis: runtime options
# --------------------------------------------------------

dt = 1.0
tf = 3.0
analysis = TransientAnalysis(theModel, dt, tf)
analysis.solve()
#model.printDetailed()
