from fem import Mesh, Model
from collections import defaultdict
from elements.truss import TrussCreator


def createModel():
    """
    The goal of one example is to create a Model. This model will have
    to include the Mesh (with its components - see below), the type of
    elements (the equations that represent its behaviour), and the
    actions on the model (boundary conditions and loading).
    """

    #--------------------------------------------------------
    #           mesh geometry: vertices, cells and nodesets
    #--------------------------------------------------------
    vertices = [
        (0.0, 0.0),
        (2.0, 0.0),
        (0.0, 0.8),
        (2.0, 1.0),
        (0.0, 2.0),
        (2.0, 2.0)]

    cells = [
        (0, 2),
        (0, 3),
        (2, 3),
        (1, 3),
        (2, 4),
        (2, 5),
        (3, 5),
        (4, 5),
        (3, 4)]

    # vsets, with names and vertex labels
    # careful: if only one vertex, use a comma after the vertexlabel
    vsets = {}
    vsets["support"] = (0, 1)
    vsets["tip"] = (3,)

    # csets, with names and cell labels
    # careful: if only one cell, use a comma after the cell label
    cellsets = {}
    cellsets["woodbars"] = (0, 1, 2, 3)
    cellsets["steelbars"] = (4, 5, 6, 7, 8)


    # --------------------------------------------------------
    #               Create global data structures
    # --------------------------------------------------------
    theMesh = Mesh(vertices, cells, vsets, cellsets)
    theMesh.print()

    # --------------------------------------------------------
    #  Model: elements types, constraints, and loading on the mesh
    # --------------------------------------------------------

    # define and assign the element types
    # 'E' is Young's modulus of the material
    # 'A' is the cross section area
    elmtTypes = {} # do not change this line
    elmtTypes["steelbars"] = TrussCreator({"E" : 210.0e9, "A" : 0.02})
    elmtTypes["woodbars"]  = TrussCreator({"E" : 210.0e9, "A" : 0.01})

    # constrain a vertexset: (doflabel, value) for all nodes in the set
    constraints = defaultdict(list) # do not change this line
    constraints["support"].append((0, 0.0))
    constraints["support"].append((1, 0.0))

    # load a vertexset: (doflabel, value) for all nodes in the set
    loading = defaultdict(list) # do not change this line
    loading["tip"].append((0, 10.0))

    theModel = Model(theMesh, elmtTypes, constraints, loading)
    return theModel
