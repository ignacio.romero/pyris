#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Thu Sep  1 16:49:17 2022
#
#@author: ignacio romero
#"""

import sys
sys.path.append('../../../src')


from exampleTruss import createModel
from fem import StaticLinearAnalysis

model = createModel()
model.print()
analysis = StaticLinearAnalysis(model)
analysis.solve()
model.printDetailed()
