#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#"""
#Created on Thu Sep  1 16:49:17 2022
#
#@author: ignacio romero
#"""
import sys
sys.path.append('../../src')


# this example is a stationary heat problem.
# To run it, change the keyword "False" to "True" in the next line
from exampleHeat import createModel
from fem import TransientAnalysis

model = createModel()
dt = 0.1
tf = 2.0
analysis = TransientAnalysis(model, dt, tf)
analysis.solve()
model.printDetailed()
