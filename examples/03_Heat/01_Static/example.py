import sys
sys.path.append('../../../src')

from fem import Mesh, Model, StaticLinearAnalysis
from collections import defaultdict
from elements.poisson import PoissonCreator

def createModel():
    """
    The goal of one example is to create a Model. This model will have
    to include the Mesh (with its components - see below), the type of
    elements (the equations that represent its behaviour), and the
    actions on the model (boundary conditions and loading).
    """

    #--------------------------------------------------------
    #           mesh geometry: vertices, cells and nodesets
    #--------------------------------------------------------
    vertices = [
        (0.0, 0.0),
        (1.0, 0.0),
        (0.0, 1.0),
        (1.0, 1.2),
        (0.0, 2.0),
        (1.0, 2.0),
        (0.0, 3.0),
        (1.0, 3.0),
        (2.0, 2.0),
        (2.0, 3.0)]

    cells = [
        (0, 1, 2),
        (1, 3, 2),
        (2, 3, 4),
        (3, 5, 4),
        (4, 5, 6),
        (5, 7, 6),
        (5, 8, 9),
        (5, 9, 7)]

    # vertexsets, with names and vertex labels
    # careful: if only one vertex, use a comma after the vertexlabel
    vertexsets = {}
    vertexsets["bottom"] = (0, 1)
    vertexsets["nodes on the side"] = (8, 9)

    cellsets = {}
    cellsets["triangles"] = (0, 1, 2, 3, 4, 5, 6, 7)


    # --------------------------------------------------------
    #               Create global data structures
    # --------------------------------------------------------
    theMesh = Mesh(vertices, cells, vertexsets, cellsets)
    theMesh.print()


    # --------------------------------------------------------
    #  Model: elements types, constraints, and loading on the mesh
    # --------------------------------------------------------

    # assign one element type to each cellset
    elmtTypes = {}
    elmtTypes["triangles"] = PoissonCreator({"conductivity" : 1.0,
                                             "heat" : 3.0,
                                             "capacity" : 5.0})

    # constrain a nodeset: (doflabel, value) for all nodes in the set
    constraints = defaultdict(list) # do not change this line
    constraints["bottom"].append((0, 0.0))
    constraints["nodes on the side"].append((0, 4.0))

    # load a nodeset: (doflabel, value) for all nodes in the set
    loading = defaultdict(list) # do not change this line
    #loading["top"].append((0, 10.0))

    theModel = Model(theMesh, elmtTypes, constraints, loading)
    return theModel


def plotSolution(theModel):
    '''
    Plots in the python output the fields that have been saved on the file solution.vtu.
    Note that pyvista must be installed for this function to work.
    '''
    p1 =pv.read('solution.vtu');
    print(p1.array_names)
    p1.plot(scalars=p1.array_names[0], component=0, cpos='xy', show_scalar_bar=True, show_edges=True)
    p1.plot(scalars=p1.array_names[1], component=0, cpos='xy', show_scalar_bar=True, show_edges=True)
    p1.plot(scalars=p1.array_names[2], component=0, cpos='xy', show_scalar_bar=True, show_edges=True)
    p1.plot(color='white', show_scalar_bar=False, show_edges=True)


def main():
    model = createModel()
    analysis = StaticLinearAnalysis(model)
    analysis.solve()
    model.printDetailed()


if __name__ == "__main__":
    main()
