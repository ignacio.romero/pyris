# pyris #

This is version 1.0 of pyris, a simple finite element code developed for teaching. It might contain bugs.


### Developers ###

* I. Romero (ignacio.romero@upm.es)



### Additional python packages ###

To run pyris, a few python packages need to be installed:
* numpy: scientific manipulation of arrays, vectores, matrices, etc.
* scipy
* matplotlib
* pyevtk

The list of these modules is in src/requirements.txt. You can install them with the terminal command:

   pip install -r src/requirements.txt
